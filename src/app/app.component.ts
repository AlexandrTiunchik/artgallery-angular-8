import { Component, OnInit } from '@angular/core';
import { IButtonConfigInterface } from './root/interfaces/IButtonConfig.interface';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IAppReducer } from './root/storage/app-storage/reducers/app.reducer';
import { LanguageUtil } from './root/util/language.util';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageUtil } from './root/util/local-storage.util';
import { appConfigs } from './root/configs/app.config';
import * as AppAction from './root/storage/app-storage/actions/app.action';
import { ButtonService } from './root/services/button.service';
import { UserRole } from './root/models/role.model';
import { IUserRoleInterface } from './root/interfaces/IUserRole.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  menuButton = [
    {
      icon: 'mainPage',
      text: 'Main Page'
    },
    {
      icon: 'work',
      text: 'Work'
    },
    {
      icon: 'shop',
      text: 'Shop'
    },
    {
      icon: 'gallery',
      text: 'Gallery'
    },
  ];

  userRole: IUserRoleInterface;

  currentLanguage: string;

  storeApp$: Observable<IAppReducer>;

  headerButtonsConfigs$: Observable<IButtonConfigInterface[]>;

  constructor(private store: Store<{appReducer: IAppReducer}>,
              private translateService: TranslateService,
              private buttonService: ButtonService) {
    this.storeApp$ = this.store.select('appReducer');
    this.userRole = UserRole;
  }

  ngOnInit(): void {
    this.currentLanguage = LocalStorageUtil.getUserToken(appConfigs.language);
    this.store.dispatch(new AppAction.AppInit());
    this.headerButtonsConfigs$ = this.buttonService.findButtons({prop: 'position', value: 'header'});

  }

  changeLanguage(language: string): void {
    LanguageUtil.setLanguage(this.translateService, language);
    this.currentLanguage = language;
  }
}

