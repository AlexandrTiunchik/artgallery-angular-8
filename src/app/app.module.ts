import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injector, NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { ButtonService } from './root/services/button.service';
import { JwtInterceptorService } from './root/services/jwt-interceptor.service';
import { RootApiService } from './root/services/root.api.service';
import { Storage } from './root/storage/storage';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageUtil } from './root/util/language.util';
import { UiModule } from './ui/ui.module';
import { FormlyModule } from '@ngx-formly/core';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { translateLoader } from './root/translation/translate.config';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { TranslationInterceptorService } from './root/translation/services/translation-interceptor.service';
import { ApiService } from './root/services/api.service';
import { MatSidenavModule } from '@angular/material';
import { LogoutFunction } from './root/functions/logout.function';
import { SignInFunction } from './root/functions/signIn.function';
import { appInitializerFactory } from './root/translation/app-initializer-factory';
import { GoToMyProfileFunction } from './root/functions/go-to-my-profile.function';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    UiModule,
    AppRoutingModule,
    CommonModule,
    HttpClientModule,
    StoreModule.forRoot(Storage.reducers),
    EffectsModule.forRoot(Storage.effects),
    FormlyModule.forRoot(),
    StoreDevtoolsModule.instrument({}),
    TranslateModule.forRoot({
      loader: translateLoader
    }),
    MatSidenavModule,
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: appInitializerFactory,
    //   deps: [TranslateService, Injector],
    //   multi: true
    // },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TranslationInterceptorService,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    },
    ApiService,
    ButtonService,
    RootApiService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(private translate: TranslateService,
              private buttonService: ButtonService) {
    LanguageUtil.setDefaultLanguage(translate);
    buttonService.createButtonsConfigs(this.appFunctions);
  }

  private appFunctions = [
    GoToMyProfileFunction,
    LogoutFunction,
    SignInFunction
  ];
}
