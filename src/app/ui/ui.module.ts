import { NgModule } from '@angular/core';
import {
  MatButtonModule,
  MatButtonToggleModule,
  MatInputModule,
  MatTabsModule,
  MatProgressSpinnerModule,
  MatSidenavModule,
  MatBadgeModule,
  MatMenuModule,
  MatDialogModule,
  MatSelectModule,
  MatChipsModule,
  MatRadioModule,
  MatAutocompleteModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatCardModule,
  MatListModule
} from '@angular/material';
import { SpinnerInitializerComponent } from './spinner/spinner-initializer/spinner-initializer.component';
import { HeaderComponent } from './header/header.component';
import { TranslateModule } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { IconModule } from './icon/icon.module';
import { MessageModalComponent } from './message-modal/message-modal.component';
import { FieldsetModule } from 'primeng/primeng';

@NgModule({
  declarations: [
    SpinnerInitializerComponent,
    HeaderComponent,
    MessageModalComponent
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatMenuModule,
    MatProgressSpinnerModule,
    MatBadgeModule,
    TranslateModule,
    MatSidenavModule,
    RouterModule,
    MatDialogModule,
    IconModule,
    MatSelectModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatRadioModule,
    MatDatepickerModule,
    FieldsetModule,
    MatExpansionModule,
    MatCardModule,
  ],
  exports: [
    MatInputModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatTabsModule,
    MatMenuModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    SpinnerInitializerComponent,
    HeaderComponent,
    IconModule,
    MatBadgeModule,
    MatChipsModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatSelectModule,
    MatRadioModule,
    MatAutocompleteModule,
    MessageModalComponent,
    FieldsetModule,
    MatExpansionModule,
    MatCardModule,
    MatListModule
  ]
})
export class UiModule {
  constructor() {}
}
