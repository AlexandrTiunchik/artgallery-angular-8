import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'app-loader-init',
  templateUrl: './spinner-initializer.component.html',
  styleUrls: ['./spinner-initializer.style.scss']
})
export class SpinnerInitializerComponent {
  /**
   * Visible property for component
   */
  @Input()
  isVisible: boolean;

  /**
   * Z-index for component
   */
  @Input() zIndex: number = 100;

  /**
   * Set visible type for component
   */
  @HostBinding('style.visibility') get visible(): string {
    return this.isVisible ? 'visible' : 'hidden';
  }

  /**
   * Set z-index for component
   */
  @HostBinding('style.z-index') get index(): number {
    return this.zIndex;
  }
}
