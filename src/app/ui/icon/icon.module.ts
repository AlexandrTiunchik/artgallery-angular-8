import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatIconRegistry, MatIconModule } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

interface IIcons {
  iconName: string;
  sanitizerValue: string;
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatIconModule
  ],
  exports: [
    MatIconModule
  ]
})

export class IconModule {
  icons: IIcons[] = [
    {
      iconName: 'logo',
      sanitizerValue: 'assets/icon/logo.svg'
    },
    {
      iconName: 'sidenav',
      sanitizerValue: 'assets/icon/sidenav.svg'
    },
    {
      iconName: 'shop',
      sanitizerValue: 'assets/icon/shop.svg'
    },
    {
      iconName: 'gallery',
      sanitizerValue: 'assets/icon/gallery.svg'
    },
    {
      iconName: 'mainPage',
      sanitizerValue: 'assets/icon/mainPage.svg'
    },
    {
      iconName: 'news',
      sanitizerValue: 'assets/icon/news.svg'
    },
    {
      iconName: 'work',
      sanitizerValue: 'assets/icon/work.svg'
    },
    {
      iconName: 'profile',
      sanitizerValue: 'assets/icon/profile.svg'
    },
    {
      iconName: 'message',
      sanitizerValue: 'assets/icon/message.svg'
    },
    {
      iconName: 'search',
      sanitizerValue: 'assets/icon/search.svg'
    },
    {
      iconName: 'pop',
      sanitizerValue: 'assets/icon/pop.svg'
    },
    {
      iconName: 'dislike',
      sanitizerValue: 'assets/icon/dislike.svg'
    },
    {
      iconName: 'like',
      sanitizerValue: 'assets/icon/like.svg'
    },
    {
      iconName: 'hot',
      sanitizerValue: 'assets/icon/hot.svg'
    },
    {
      iconName: 'compact_view',
      sanitizerValue: 'assets/icon/compact_view.svg'
    },
    {
      iconName: 'classic_view',
      sanitizerValue: 'assets/icon/classic_view.svg'
    },
    {
      iconName: 'art',
      sanitizerValue: 'assets/icon/art.svg'
    },
    {
      iconName: 'new',
      sanitizerValue: 'assets/icon/new.svg'
    },
    {
      iconName: 'blog',
      sanitizerValue: 'assets/icon/blog.svg'
    }
  ];
  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer) {
    this.icons.map( (icon) => {
      iconRegistry.addSvgIcon(icon.iconName, sanitizer.bypassSecurityTrustResourceUrl(icon.sanitizerValue));
    });
  }
}
