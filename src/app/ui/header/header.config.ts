import { IHeaderInterface } from './IHeader.interface';

export const HeaderGuestConfig: IHeaderInterface  = {
  logoText: 'ArtGallery',
  button: [
    {
      text: 'Sign',
      color: null,
      link: 'authorization'
    },
    {
      text: 'Registry',
      color: 'primary',
      link: 'authorization'
    },
  ]
};

export const HeaderAdminConfig: IHeaderInterface  = {
  logoText: 'ArtGallery',
  button: [
    {
      text: 'Logout',
      color: null,
    },
    {
      text: 'Profile',
      color: 'primary',
      link: 'user'
    },
  ]
};

export const HeaderUserConfig: IHeaderInterface  = {
  logoText: 'ArtGallery',
  button: [
    {
      text: 'Message',
      color: 'primary',
      badge: '4',
      icon: 'message',
      link: 'message'
    },
    {
      text: 'Profile',
      color: 'primary',
      icon: 'profile',
      popUpItem: [
        {
          text: 'User',
        },
        {
          text: 'Log out'
        }
      ]
    },
  ]
};

