interface PopUpItem {
  text: string;
}

interface HeaderButton {
  text: string;
  color?: string;
  icon?: string;
  badge?: string;
  link?: string;
  popUpItem?: PopUpItem[];
}

export interface IHeaderInterface {
  logoText?: string;
  button: HeaderButton[];
}
