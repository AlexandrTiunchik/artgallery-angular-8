import { Component, HostBinding, Input } from '@angular/core';
import { IButtonConfigInterface } from '../../root/interfaces/IButtonConfig.interface';
import { IAppReducer } from '../../root/storage/app-storage/reducers/app.reducer';
import { IUserRoleInterface } from '../../root/interfaces/IUserRole.interface';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  /**
   * User role list
   */
  @Input() userRole: IUserRoleInterface;
  /**
   * Header settings from store
   */
  @Input() headerSettings: IAppReducer;
  /**
   * Sidenav element for component
   */
  @Input() sidenav;
  /**
   * HeaderModel for component
   */
  @Input() headerModel: IButtonConfigInterface[];
  /**
   * Visible for component
   */
  @Input() isVisible: boolean = true;
  /**
   * Set visible type for component
   */
  @Input() zIndex: number = 100;
  /**
   * Set visibility for header
   * @returns {string}
   */
  @HostBinding('style.visibility') get visible(): string {
    return this.isVisible ? 'visible' : 'hidden';
  }
  /**
   * Set z-index for header
   * @returns {number}
   */
  @HostBinding('style.z-index') get index(): number {
    return this.zIndex;
  }
}
