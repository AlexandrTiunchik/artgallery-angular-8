import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProfileSmartComponent } from './profile-smart.component';

const profileRoutes: Routes = [
  {
    path: '',
    component: ProfileSmartComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(profileRoutes)
  ]
})
export class ProfileRoutingModule {}
