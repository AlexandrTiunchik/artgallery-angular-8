import { NgModule } from '@angular/core';
import { ProfileSmartComponent } from './profile-smart.component';
import { ProfileRoutingModule } from './profile-routing.module';

@NgModule({
  declarations: [
    ProfileSmartComponent
  ],
  imports: [
    ProfileRoutingModule
  ]
})
export class ProfileModule { }
