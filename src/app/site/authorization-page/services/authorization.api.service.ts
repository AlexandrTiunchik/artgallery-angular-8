import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ApiService } from '@root';
import { Observable } from 'rxjs';

@Injectable()
export class AuthorizationApiService extends ApiService {

  private readonly baseUrl: string = 'https://webapp-190806220116.azurewebsites.net/api/v1/identity';

  constructor(http: HttpClient) {
    super(http);
  }

  registrationUserApi(url, data: any): Observable<any> {
    return this.post(this.baseUrl + url, data);
  }

  loginUserApi(url, data: any): Observable<any> {
    return this.post(this.baseUrl + url, data);
  }
}
