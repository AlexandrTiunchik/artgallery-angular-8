import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {IAppReducer} from '../../../root/storage/app-storage/reducers/app.reducer';
import {Store} from '@ngrx/store';
import {Observable} from 'rxjs';

@Injectable()
export class AuthorizationGuardService implements CanActivate {
  constructor(private router: Router,
              private store: Store<{appReducer: IAppReducer}>) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.checkAccess();
  }

  checkAccess(): Observable<any> {
    return this.store.select(store => {
      return !store.appReducer.token;
    });
  }
}
