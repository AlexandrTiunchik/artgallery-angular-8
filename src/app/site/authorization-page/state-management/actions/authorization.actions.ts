import { IActionInterface } from '@root';

const base = '[Authorization]';

export const LOGIN = `${base}_login`;
export const ERROR = `${base}_error`;
export const REGISTRATION = `${base}_registration`;

export class Login implements IActionInterface {
  readonly type = LOGIN;
  constructor(public payload: any) {}
}

export class AuthorizationError implements IActionInterface {
  readonly type = ERROR;
  constructor(public payload: any) {}
}

export class Registration implements IActionInterface {
  readonly type = ERROR;
  constructor(public payload: any) {}
}

export type AuthorizationActions =
  Login
  | AuthorizationError
  | Registration;
