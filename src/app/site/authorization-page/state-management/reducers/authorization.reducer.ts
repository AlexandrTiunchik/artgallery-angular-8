import * as AuthorizationActions from '../actions/authorization.actions';

export interface IAuthorizationReducer {
  error: any;
}

const initialState: IAuthorizationReducer = {
  error: null,
};

export function AuthorizationReducer(state = initialState, action: AuthorizationActions.AuthorizationActions) {
  switch (action.type) {
    case AuthorizationActions.ERROR:
      return {
        ...state,
        error: action.payload
      };

    default: return state;
  }
}
