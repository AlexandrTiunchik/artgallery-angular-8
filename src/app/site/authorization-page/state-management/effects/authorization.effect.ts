import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import {catchError, map, switchMap, withLatestFrom} from 'rxjs/operators';
import * as AuthorizationActions from '../actions/authorization.actions';
import { IActionInterface } from '@root';
import { LoginModel } from '../../model/login.model';
import { RegistrationModel } from '../../model/registration.model';
import { AuthorizationApiService } from '../../services/authorization.api.service';
import * as AppActions from '@root';
import { Store } from '@ngrx/store';
import { IAppReducer } from '../../../../root/storage/app-storage/reducers/app.reducer';
import { IAuthorizationReducer } from '../reducers/authorization.reducer';
import { Router } from '@angular/router';


@Injectable()
export class AuthorizationEffect {
  constructor(
    private actions$: Actions,
    private router: Router,
    private authorizationApiService: AuthorizationApiService,
    private store: Store<{ appReducer: IAppReducer, authReducer: IAuthorizationReducer }>
  ) { }

  @Effect()
  login$ = this.actions$.pipe(
    ofType(AuthorizationActions.LOGIN),
    map((action: IActionInterface) => action.payload),
    withLatestFrom(this.store.select(state => state.appReducer.lastRoute)),
    switchMap(([payload, lastRoute]: [LoginModel, string]) => {
      this.store.dispatch(new AppActions.ShowInitialSpinner(true));
      return this.authorizationApiService.loginUserApi('/login', payload).pipe(
        switchMap(response => {

          if (lastRoute) {
            this.router.navigate([lastRoute]).then();
          } else {
            this.router.navigate(['/']).then();
          }

          return [
            new AppActions.SaveUserToken(response),
            new AppActions.GetAuthorizedUserData(),
            new AppActions.HideInitialSpinner()
          ];
        }),
        catchError((error) => {
          return [
            new AuthorizationActions.AuthorizationError(error.error.errors),
            new AppActions.HideInitialSpinner()
          ];
        }),
      );
    })
  );

  @Effect()
  registration$ = this.actions$.pipe(
    ofType(AuthorizationActions.REGISTRATION),
    map((action: IActionInterface) => action.payload),
    switchMap( (payload: RegistrationModel) => {
      this.store.dispatch(new AppActions.ShowInitialSpinner(true));
      return this.authorizationApiService.registrationUserApi('/register', payload).pipe(
        switchMap( response => {
          return [
            new AppActions.SaveUserToken(response),
            new AppActions.HideInitialSpinner()
          ];
        }),
        catchError( (error) => {
          return [
            new AuthorizationActions.AuthorizationError(error.error.errors),
            new AppActions.HideInitialSpinner()
          ];
        }),
      );
    })
  );
}
