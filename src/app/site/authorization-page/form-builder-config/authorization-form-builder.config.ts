import {emailPattern} from '@root';
import {IFormlyBuilderInterface} from '@root';

/**
 * Login fields configs
 */
export const loginFormConfig: IFormlyBuilderInterface[] = [
  {
    key: 'login',
    type: 'input',
    translation: {
      label: 'auth.login.name',
      placeholder: '',
    },
    templateOptions: {
      required: true
    },
    validators: [
      {type: 'required', validMessage: 'auth.validation.requre'}
    ]
  },
  {
    key: 'password',
    type: 'input',
    translation: {
      label: 'auth.password',
      placeholder: '',
    },
    templateOptions: {
      required: true
    },
    validators: [
      {type: 'required', validMessage: 'auth.validation.requre'}
    ]
  },
];

/**
 * Registration fields configs
 */
export const registrationFormConfig: IFormlyBuilderInterface[] = [
  {
    key: 'login',
    type: 'input',
    translation: {
      label: 'auth.login.name',
      placeholder: '',
    },
    templateOptions: {
      required: true
    },
    validators: [
      { type: 'minLength', value: 6, validMessage: 'auth.validation.password.more' },
      { type: 'maxLength', value: 12, validMessage: 'auth.validation.password.less' },
      { type: 'required', validMessage: 'auth.validation.requre' }
    ]
  },
  {
    key: 'email',
    type: 'input',
    validators: [
      { type: 'pattern', value: emailPattern, validMessage: 'auth.validation.email' },
      { type: 'required', validMessage: 'auth.validation.requre' }
    ],
    templateOptions: {
      required: true
    },
    translation: {
      label: 'auth.email',
      placeholder: ''
    }
  },
  {
    key: 'password',
    type: 'input',
    validators: [
      { type: 'maxLength', value: 12, validMessage: 'auth.validation.password.less' },
      { type: 'minLength', value: 6, validMessage: 'auth.validation.password.more' },
      { type: 'required', validMessage: 'auth.validation.requre' }
    ],
    templateOptions: {
      required: true
    },
    translation: {
      label: 'auth.password',
      placeholder: ''
    }
  },
  {
    key: 'confirmPassword',
    type: 'input',
    validators: [
      { type: 'confirmPassword', validMessage: 'auth.validation.confirm.password' },
      { type: 'required', validMessage: 'auth.validation.requre' }
    ],
    templateOptions: {
      required: true
    },
    translation: {
      label: 'auth.confirm.password',
      placeholder: ''
    }
  }
];
