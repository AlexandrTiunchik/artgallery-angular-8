/**
 * Model for registration form
 */
export class RegistrationModel {
  login: string;
  email: string;
  password: string;
  confirmPassword: string;
}
