/**
 * Model for login form
 */
export class LoginModel {
  login: string;
  password: string;
}
