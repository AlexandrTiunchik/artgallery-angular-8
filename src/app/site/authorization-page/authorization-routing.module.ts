import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthorizationSmartComponent } from './authorization-smart.component';
import { AuthorizationGuardService } from './services/authorization-guard.service';

const authorizationRoutes: Routes = [
  {
    path: '',
    component: AuthorizationSmartComponent,
    canActivate: [AuthorizationGuardService]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(authorizationRoutes)
  ]
})
export class AuthorizationRoutingModule {}
