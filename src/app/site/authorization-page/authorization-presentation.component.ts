import {ChangeDetectionStrategy, Component, EventEmitter, Output} from '@angular/core';
import { LoginModel } from './model/login.model';
import { RegistrationModel } from './model/registration.model';


@Component({
  selector: 'app-authorization-presentation',
  templateUrl: './authorization-presentation.component.html',
  styleUrls: ['./authorization-presentation.style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthorizationPresentationComponent {
  /**
   * Output submitLogin function
   */
  @Output() submitLogin = new EventEmitter<LoginModel>();

  /**
   * Output submitRegistration function
   */
  @Output() submitRegistration = new EventEmitter<any>();

  /**
   * Emit registration function
   * @param registrationData
   */
  registrationData(registrationData: RegistrationModel): void {
    this.submitRegistration.emit(registrationData);
  }

  /**
   * Emit login function
   * @param loginData
   */
  loginData(loginData: LoginModel): void {
    this.submitLogin.emit(loginData);
  }
}
