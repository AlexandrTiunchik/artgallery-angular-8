import { NgModule } from '@angular/core';
import { AuthorizationPresentationComponent } from './authorization-presentation.component';
import { AuthorizationSmartComponent } from './authorization-smart.component';
import { AuthorizationRoutingModule } from './authorization-routing.module';
import { LoginComponent } from './component/login/login.component';
import { RegistrationComponent } from './component/registration/registration.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { UiModule } from '../../ui/ui.module';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { FormlyModule } from '@ngx-formly/core';
import { TranslateModule } from '@ngx-translate/core';
import { FormService } from '@root';
import { AuthorizationApiService } from './services/authorization.api.service';
import { StoreModule } from '@ngrx/store';
import { AuthorizationReducer } from './state-management/reducers/authorization.reducer';
import { EffectsModule } from '@ngrx/effects';
import { AuthorizationEffect } from './state-management/effects/authorization.effect';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { AuthorizationGuardService } from './services/authorization-guard.service';


@NgModule({
  declarations: [
    AuthorizationPresentationComponent,
    AuthorizationSmartComponent,
    LoginComponent,
    RegistrationComponent
  ],
  imports: [
    AuthorizationRoutingModule,
    UiModule,
    FormlyMaterialModule,
    ReactiveFormsModule,
    TranslateModule,
    FormlyModule.forRoot(),
    CommonModule,
    StoreModule.forFeature('authReducer', AuthorizationReducer),
    EffectsModule.forFeature([AuthorizationEffect]),
    StoreDevtoolsModule.instrument({})
  ],
  providers: [
    FormService,
    AuthorizationApiService,
    AuthorizationGuardService
  ]
})
export class AuthorizationModule {}
