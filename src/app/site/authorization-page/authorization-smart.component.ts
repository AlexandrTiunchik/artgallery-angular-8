import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LoginModel } from './model/login.model';
import { RegistrationModel } from './model/registration.model';
import * as AuthenticationActions from './state-management/actions/authorization.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'app-authorization-smart',
  template: `
    <app-authorization-presentation
      (submitLogin)="submitLogin($event)"
      (submitRegistration)="submitRegistration($event)"
    >
    </app-authorization-presentation>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthorizationSmartComponent {
  /**
   * Constructor
   * @param store
   */
  constructor(private store: Store<any>) {}

  /**
   * Submit registration form data
   * @param registrationData
   */
  submitRegistration(registrationData: RegistrationModel) {
    this.store.dispatch(new AuthenticationActions.Registration(registrationData));
  }

  /**
   * Submit login form data
   * @param loginData
   */
  submitLogin(loginData: LoginModel): void {
    this.store.dispatch(new AuthenticationActions.Login(loginData));
  }
}
