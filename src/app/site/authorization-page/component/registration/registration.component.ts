import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {RegistrationModel} from '../../model/registration.model';
import {FormGroup} from '@angular/forms';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormService} from '@root';
import {registrationFormConfig} from '../../form-builder-config/authorization-form-builder.config';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.style.scss']
})
export class RegistrationComponent implements OnInit {
  constructor(private validatorService: FormService) {}

  /**
   * Output registrationFormData function
   */
  @Output() registrationFormData = new EventEmitter<RegistrationModel>();

  /**
   * Form group
   */
  registrationForm: FormGroup;

  /**
   * Array of form config
   */
  registrationFields: FormlyFieldConfig[];

  /**
   * Login data model
   */
  registrationModel: RegistrationModel;

  /**
   * Property initialization
   */
  ngOnInit(): void {
    this.registrationModel = new RegistrationModel();
    this.registrationForm = new FormGroup({});
    this.registrationFields = this.validatorService.formBuilderFactory(registrationFormConfig);
  }

  /**
   * Submit form
   * @param formData
   */
  submitForm(formData: RegistrationModel): void {
    this.registrationFormData.emit(formData);
  }
}
