import { Component, EventEmitter, Injector, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { LoginModel } from '../../model/login.model';
import { loginFormConfig } from '../../form-builder-config/authorization-form-builder.config';
import { FormlyFieldConfig } from '@ngx-formly/core';
import { FormService } from '@root';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.style.scss']
})
export class LoginComponent implements OnInit {
  constructor(private validationService: FormService) {}

  /**
   * Output loginFormData function
   */
  @Output() loginFormData = new EventEmitter<LoginModel>();

  /**
   * Array of form config
   */
  loginFields: FormlyFieldConfig[];

  /**
   * Form group
   */
  loginForm: FormGroup;

  /**
   * Login data model
   */
  loginModel: LoginModel;

  /**
   * Property initialization
   */
  ngOnInit(): void {
    this.loginModel = new LoginModel();
    this.loginForm = new FormGroup({});
    this.loginFields = this.validationService.formBuilderFactory(loginFormConfig);
  }

  /**
   * Submit form
   * @param formData
   */
  submitForm(formData: LoginModel): void {
    this.loginFormData.emit(formData);
  }
}
