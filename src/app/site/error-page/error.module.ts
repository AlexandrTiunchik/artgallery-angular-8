import {NgModule} from '@angular/core';
import {ErrorPageComponent} from './error-presentation.compent';
import {ErrorRoutingModule} from './error-routing.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  declarations: [
    ErrorPageComponent
  ],
  imports: [
    ErrorRoutingModule,
    TranslateModule
  ]
})
export class ErrorModule { }
