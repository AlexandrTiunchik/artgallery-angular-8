import {Component} from '@angular/core';

@Component({
  selector: 'app-error-presentation',
  templateUrl: 'error-presentation.component.html',
  styleUrls: ['error-presentation.style.scss']
})
export class ErrorPageComponent {}
