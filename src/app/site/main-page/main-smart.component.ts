import { Component, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-main-smart',
  template: `<app-main-presentation></app-main-presentation>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainSmartComponent {}
