import { NgModule } from '@angular/core';
import { MainPresentationComponent } from './main-presentation.component';
import { MainRoutingModule } from './main-routing.module';
import { MainSmartComponent } from './main-smart.component'
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    MainPresentationComponent,
    MainSmartComponent
  ],
  imports: [
    MainRoutingModule,
    TranslateModule
  ]
})
export class MainModule { }
