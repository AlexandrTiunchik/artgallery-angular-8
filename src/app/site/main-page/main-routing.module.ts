import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainSmartComponent } from './main-smart.component';

const mainRouting: Routes = [
  {
    path: '',
    component: MainSmartComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(mainRouting)
  ]
})
export class MainRoutingModule {}
