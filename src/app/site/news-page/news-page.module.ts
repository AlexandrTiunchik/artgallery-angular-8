import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NewsPagePresentationComponent } from './news-page-presentation.component';
import { NewsPageRoutingModule } from './news-page-routing.module';
import { NewsPageSmartComponent } from './news-page-smart.component';
import { UiModule } from '../../ui/ui.module';
import { TranslateModule } from '@ngx-translate/core';
import { NewsReducer } from './state-menegement/reducers/news.reducers';
import { StoreModule} from '@ngrx/store';
import { EffectsModule} from '@ngrx/effects';
import { NewsEffects} from './state-menegement/effects/news.effects';
import { StoreDevtoolsModule} from '@ngrx/store-devtools';
import { FilterComponent } from './components/filter/filter.component'
import { NewsApiService } from './service/news.api.service';
import { HotNewsComponent } from './components/hot-news/hot-news.component';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    NewsPagePresentationComponent,
    NewsPageSmartComponent,
    FilterComponent,
    HotNewsComponent,
  ],
  imports: [
    NewsPageRoutingModule,
    CommonModule,
    UiModule,
    TranslateModule,
    StoreModule.forFeature('newsReducer', NewsReducer),
    EffectsModule.forFeature([NewsEffects]),
    StoreDevtoolsModule.instrument({}),
    ReactiveFormsModule
  ],
  providers: [
    NewsApiService
  ]
})

export class NewsModule { }
