import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { IAppReducer } from '../../root/storage/app-storage/reducers/app.reducer';
import * as NewsActions from './state-menegement/actions/news.actions';
import { sortModel } from './state-menegement/model/sortModel';

@Component({
  selector: 'app-news-smart',
  template: `<app-news-presentation
    [post]="(storeApp$ | async).news"
    [sortDataBySmart]="sortModel"
    (sortByViewModel)="sortByView($event)"
  >
  </app-news-presentation>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NewsPageSmartComponent implements OnInit {

  storeApp$: Observable<IAppReducer>;
  sortModel: sortModel = {
    view: 'compact'
  };

  constructor(private store: Store<{appReducer: IAppReducer}>) {
    this.storeApp$ = this.store.select('appReducer');
  }

  ngOnInit() {
    this.store.dispatch(new NewsActions.GetNews());
  }

  sortByView(type) {
    this.sortModel.view = type;
  }
}
