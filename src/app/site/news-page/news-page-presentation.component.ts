import {EventEmitter, Input, OnInit, Output} from '@angular/core';
import { Component } from '@angular/core';
import {sortModel} from './state-menegement/model/sortModel';
@Component({
  selector: 'app-news-presentation',
  templateUrl: './news-page.component-presentation.html',
  styleUrls: ['./news-page-presentation.component.scss']
})
export class NewsPagePresentationComponent implements OnInit {
  @Input() post = [];
  @Output() sortByViewModel = new EventEmitter<sortModel>();
  @Input() sortDataBySmart: sortModel;

  constructor() {}

  ngOnInit() {}

  sortByView(type) {
    this.sortByViewModel.emit(type);
  }
}
