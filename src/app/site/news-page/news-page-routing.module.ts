import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewsPageSmartComponent } from './news-page-smart.component';

const newsRoutes: Routes = [
  {
    path: '',
    component: NewsPageSmartComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(newsRoutes)
  ]
})

export class NewsPageRoutingModule {}
