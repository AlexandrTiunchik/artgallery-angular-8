import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {sortModel} from '../../state-menegement/model/sortModel';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit{
  @Output() sortByView = new EventEmitter<any>();
  @Input() sortDataBySmart: sortModel;
  toppingList: object[] = [
    {
      tag: 'Hot',
      icon: 'hot'
    },
    {
      tag: 'New',
      icon: 'new'
    },
    {
      tag: 'Art',
      icon: 'art'
    },
    {
      tag: 'Blog',
      icon: 'blog'
    },
    {
      tag:  'Work',
      icon: 'work'
    }
  ];
  changeVeiw(type) {
    console.log(type);
    this.sortByView.emit(type);
  }
  ngOnInit(): void {
    console.log(this.sortDataBySmart);
  }
}
