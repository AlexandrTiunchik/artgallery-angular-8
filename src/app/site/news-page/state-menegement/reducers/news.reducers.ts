import * as NewsActions from '../actions/news.actions';

export interface INewsReducers {
  error: any;
}

const initialState: INewsReducers = {
  error: null,
};

export function NewsReducer(state = initialState, action: NewsActions.NewsActions) {
  switch (action.type) {
    case  NewsActions.ERROR:
      return {
        ...state,
        error: action.payload
      };
    default: return state;
  }

}
