import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { IActionInterface } from '@root';
import { Router } from '@angular/router';
import * as AppActions from '@root';
import { NewsApiService } from '../../service/news.api.service';
import * as NewsActions from '../actions/news.actions';
import { catchError, map, switchMap } from 'rxjs/operators';

@Injectable()
export class NewsEffects {
  constructor(
    private actions$: Actions,
    private router: Router,
    private newsApiService: NewsApiService
  ) {}

  @Effect()
  getNewsPost$ = this.actions$.pipe(
    ofType(NewsActions.GET_NEWS),
    map((action: IActionInterface) => action.payload),
    switchMap( () => {
      return this.newsApiService.getNewsPost('/news').pipe(
        switchMap( (response) => {
          return [
            new AppActions.SaveNewsPost(response.news),
          ];
        }),
        catchError( (error) => {
          return [
            new NewsActions.Error(error.error.errors),
          ];
        }),
      );
    })
  );
}
