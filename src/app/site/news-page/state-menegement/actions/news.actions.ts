import { IActionInterface } from '@root';

const base = '[News]';

export const GET_NEWS = `${base}_get_news`;
export const ERROR = `${base}_error`;

export class GetNews implements IActionInterface {
  readonly payload;
  readonly type = GET_NEWS;
}
export class Error implements IActionInterface {
  readonly type = ERROR;
  constructor(public payload: any) {}
}

export type NewsActions =
  GetNews
  | Error;
