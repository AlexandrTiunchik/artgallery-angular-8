import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Post from '../../../fake/post.fake-data';
import { ApiService } from '@root';

@Injectable()
export class NewsApiService extends ApiService {

  private readonly baseUrl: string = 'https://webapp-190806220116.azurewebsites.net/api/v1';

  constructor(http: HttpClient) {
    super(http);
  }

  getNewsPost(url): Observable<any> {
    return this.get(this.baseUrl + url);
  }
}
