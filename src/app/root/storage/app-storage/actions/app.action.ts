import {IActionInterface} from '../../../interfaces/IAction.interface';

const base = '[ROOT]';

export const SAVE_USER_TOKEN = `${base}_save_user_token`;
export const SHOW_INITIAL_SPINNER = `${base}_show_initial_spinner`;
export const HIDE_INITIAL_SPINNER = `${base}_hide_initial_spinner`;
export const LOGOUT = `${base}_logout`;
export const SET_USER_TOKEN = `${base}_set_user_token`;
export const SAVE_BUTTONS_CONFIGS = `${base}_save_buttons_configs`;
export const SAVE_LAST_ROUTE = `${base}_save_last_route`;
export const GET_AUTHORIZED_USER_DATA = `${base}_get_authorize_user_data`;
export const SAVE_AUTHORIZED_USER_DATA = `${base}_save_authorize_user_data`;
export const APP_INIT = `${base}_app_init`;
export const SAVE_NEWS_POST = `${base}_save_news_post`;

export class SaveUserToken implements IActionInterface {
  readonly type = SAVE_USER_TOKEN;
  constructor(public payload: any) {}
}

export class GetAuthorizedUserData implements IActionInterface {
  readonly payload;
  readonly type = GET_AUTHORIZED_USER_DATA;
}

export class SaveAuthorizedUserData implements IActionInterface {
  readonly type = SAVE_AUTHORIZED_USER_DATA;
  constructor(public payload: any) {}
}

export class SetUserToken implements IActionInterface {
  readonly type = SET_USER_TOKEN;
  constructor(public payload: any) {}
}

export class Logout implements IActionInterface {
  readonly payload;
  readonly type = LOGOUT;
}

export class ShowInitialSpinner implements IActionInterface {
  readonly type = SHOW_INITIAL_SPINNER;
  constructor(public payload: any) {}
}

export class SaveButtonsConfigs implements IActionInterface {
  readonly type = SAVE_BUTTONS_CONFIGS;
  constructor(public payload: any) {}
}

export class SaveLastRoute implements IActionInterface {
  readonly type = SAVE_LAST_ROUTE;
  constructor(public payload: string) {}
}

export class HideInitialSpinner implements IActionInterface {
  readonly payload;
  readonly type = HIDE_INITIAL_SPINNER;
}

export class AppInit implements IActionInterface {
  readonly payload;
  readonly type = APP_INIT;
}

export class SaveNewsPost implements IActionInterface {
  readonly type = SAVE_NEWS_POST;
  constructor(public payload: any) {}
}

export type AppAction =
  SaveUserToken
  | HideInitialSpinner
  | Logout
  | SetUserToken
  | ShowInitialSpinner
  | SaveButtonsConfigs
  | SaveLastRoute
  | GetAuthorizedUserData
  | SaveAuthorizedUserData
  | AppInit
  | SaveNewsPost;
