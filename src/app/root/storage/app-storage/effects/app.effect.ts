import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { UserRole } from '../../../models/role.model';
import { delay, map, switchMap } from 'rxjs/operators';
import { AuthorizedUserDataModel } from '../../../models/authorizedUserData.model';
import { RootApiService } from '../../../services/root.api.service';
import * as AppActions from '../actions/app.action';
import { IActionInterface } from '../../../interfaces/IAction.interface';
import { LocalStorageUtil } from '../../../util/local-storage.util';
import { appConfigs } from '../../../configs/app.config';
import { Store } from '@ngrx/store';
import { IAppReducer } from '../reducers/app.reducer';
import { IAuthorizationReducer } from '../../../../site/authorization-page/state-management/reducers/authorization.reducer';

@Injectable()
export class AppEffect {
  constructor(private action$: Actions,
              private rootApiService: RootApiService,
              private store: Store<{appReducer: IAppReducer, authReducer: IAuthorizationReducer}>) {}

  @Effect()
  saveUserToken$ = this.action$.pipe(
    ofType(AppActions.SAVE_USER_TOKEN),
    map((action: IActionInterface) => action.payload),
    switchMap( (payload: {token: string}) => {
      LocalStorageUtil.setUserToken(appConfigs.tokenItemName, payload.token);
      return [
        new AppActions.SetUserToken(payload.token)
      ];
    })
  );

  @Effect()
  logout$ = this.action$.pipe(
    ofType(AppActions.LOGOUT),
    switchMap(() => {
      LocalStorageUtil.removeUserToken(appConfigs.tokenItemName);
      return [
        new AppActions.SetUserToken(null),
        new AppActions.ShowInitialSpinner(true),
        new AppActions.SaveAuthorizedUserData({role: UserRole.GUEST} as AuthorizedUserDataModel),
        new AppActions.HideInitialSpinner()
      ];
    })
  );

  @Effect()
  initSpinner$ = this.action$.pipe(
    ofType(AppActions.HIDE_INITIAL_SPINNER),
    delay(2000),
    switchMap(() => {
      return [
        new AppActions.ShowInitialSpinner(false)
      ];
    })
  );

  @Effect()
  getInfoAboutAuthorizedUser$ = this.action$.pipe(
    ofType(AppActions.GET_AUTHORIZED_USER_DATA),
    switchMap(() => {
      this.store.dispatch(new AppActions.ShowInitialSpinner(true));
      return this.rootApiService.getAuthorizedUserInfo('/auth-user').pipe(
        switchMap((payload: AuthorizedUserDataModel) => {
          return [
            new AppActions.SaveAuthorizedUserData(payload),
            new AppActions.HideInitialSpinner()
          ];
        })
      );
    })
  );

  @Effect()
  appInit$ = this.action$.pipe(
    ofType(AppActions.APP_INIT),
    switchMap(() => {
      const actions = [];

      if (LocalStorageUtil.getUserToken(appConfigs.tokenItemName)) {
        actions.push(new AppActions.SaveUserToken({ token: LocalStorageUtil.getUserToken(appConfigs.tokenItemName) }));
        actions.push(new AppActions.GetAuthorizedUserData());
      }

      return actions;
    })
  );
}
