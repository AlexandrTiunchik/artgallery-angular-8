import { AuthorizedUserDataModel } from '../../../models/authorizedUserData.model';
import * as AppAction from '../actions/app.action';
import { IButtonConfigInterface } from '../../../interfaces/IButtonConfig.interface';

export interface IAppReducer {
  token: string;
  isInitialSpinnerVisible: boolean;
  buttonsConfigs: IButtonConfigInterface[];
  lastRoute: string;
  authorizedUserData: AuthorizedUserDataModel;
  news: [];
}

const initialState: IAppReducer = {
  token: null,
  isInitialSpinnerVisible: false,
  buttonsConfigs: null,
  lastRoute: null,
  authorizedUserData: { role: 'guest' },
  news: null
};

export function AppReducer(state = initialState, action: AppAction.AppAction) {
  switch (action.type) {
    case AppAction.SET_USER_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case AppAction.SHOW_INITIAL_SPINNER:
      return {
        ...state,
        isInitialSpinnerVisible: action.payload
      };
    case AppAction.SAVE_BUTTONS_CONFIGS:
      return {
        ...state,
        buttonsConfigs: action.payload
      };
    case AppAction.SAVE_LAST_ROUTE:
      return {
        ...state,
        lastRoute: action.payload
      };
    case AppAction.SAVE_AUTHORIZED_USER_DATA:
      return {
        ...state,
        authorizedUserData: action.payload
      };
    case AppAction.SAVE_NEWS_POST:
      return {
        ...state,
        news: action.payload
      };

    default: return state;
  }
}
