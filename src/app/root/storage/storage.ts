import { AppReducer } from './app-storage/reducers/app.reducer';
import { AppEffect } from './app-storage/effects/app.effect';


export const Storage = {
  reducers: {
    appReducer: AppReducer
  },
  effects: [
    AppEffect
  ]
};
