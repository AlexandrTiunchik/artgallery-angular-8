import {Injectable} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {IFormlyBuilderInterface, IValidatorInterface} from '../interfaces/IFormlyBuilder.interface';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {Observable} from 'rxjs';
import {FormlyValidator} from '../validators/formly.validator';

/**
 * Service for form validation
 */
@Injectable()
export class FormService {
  constructor(private translate: TranslateService) {}

  /**
   * Map with validation functions
   */
  private validatorFunctionsMap = new Map<string, Function>([
    ['minLength', FormlyValidator.minLengthValidator],
    ['maxLength', FormlyValidator.maxLengthValidator],
    ['pattern', FormlyValidator.patternValidator],
    ['confirmPassword', FormlyValidator.confirmPasswordValidator],
    ['required', FormlyValidator.requireValidator]
  ]);

  /**
   * Form builder factory
   * This function
   * @param formConfig
   */
  public formBuilderFactory(formConfig: IFormlyBuilderInterface[]): FormlyFieldConfig[] {
    const formBuilder = [];

    formConfig.forEach((formField: IFormlyBuilderInterface) => {
      formBuilder.push(this.checkFormlyType(formField));
    });

    return formBuilder;
  }

  /**
   * Check form type
   * @param formField
   */
  private checkFormlyType(formField: IFormlyBuilderInterface) {
    let formConfig: FormlyFieldConfig;
    let validatorsConfig = [];

    switch (formField.type) {
      case 'input':
        formConfig = this.inputBuilder(formField);
        validatorsConfig = this.getValidationFunction(formField);
        formConfig.validators = validatorsConfig;
        break;
      default:
        break;
    }

    return formConfig;
  }

  /**
   * Create input form config
   * @param formField
   */
  private inputBuilder(formField: IFormlyBuilderInterface): FormlyFieldConfig {
    return {
      key: formField.key,
      type: formField.type,
      templateOptions: {
        required: formField.templateOptions.required || false,
      },
      expressionProperties: {
        'templateOptions.label': this.translate.stream(formField.translation.label || ' '),
        'templateOptions.placeholder': this.translate.stream(formField.translation.placeholder || ' '),
      },
    };
  }

  /**
   * Build expression and validation message for field
   * @param formField
   */
  private getValidationFunction(formField: FormlyFieldConfig): any {
    const validators = {};

    formField.validators.forEach((validatorType: IValidatorInterface) => {
      if (this.validatorFunctionsMap.has(validatorType.type)) {
        let validatorTranslate: string;
        this.validationMessage(validatorType).subscribe(vul => validatorTranslate = vul);
        validators[validatorType.type] = {
          expression: this.validatorFunctionsMap.get(validatorType.type)(validatorType.value),
          message: () => validatorTranslate
        };
      }
      return null;
    });

    return validators;
  }

  /**
   * Get translation for validation message
   * @param formField
   */
  private validationMessage(formField: IValidatorInterface): Observable<string> {
    return this.translate.get(formField.validMessage);
  }
}
