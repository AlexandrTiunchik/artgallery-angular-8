import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable()
export class RootApiService extends ApiService {

  private readonly userUrl: string = 'https://webapp-190806220116.azurewebsites.net/api/v1/user';

  constructor(http: HttpClient) {
    super(http);
  }

  getAuthorizedUserInfo(url): Observable<any> {
    return this.get(this.userUrl + url);
  }
}
