import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { appConfigs } from '../configs/app.config';
import { LocalStorageUtil } from './../util/local-storage.util';

/**
 * Set header for bearer token
 */
@Injectable()
export class JwtInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reqs = req.clone({
      headers: req.headers.set('Authorization', 'Bearer ' + LocalStorageUtil.getUserToken(appConfigs.tokenItemName))
    });

    return next.handle(reqs);
  }
}
