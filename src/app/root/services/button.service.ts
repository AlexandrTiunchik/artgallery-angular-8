import { Injectable, Injector } from '@angular/core';
import { IButtonConfigInterface } from '../interfaces/IButtonConfig.interface';
import { Store } from '@ngrx/store';
import * as AppActions from '../storage/app-storage/actions/app.action';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';
import { IAppReducer } from '../storage/app-storage/reducers/app.reducer';

@Injectable()
export class ButtonService {
  constructor(private injector: Injector,
              private translateService: TranslateService,
              private store: Store<{appReducer: IAppReducer}>) {}

  /**
   * Create and save buttons configs
   * @param func
   */
  createButtonsConfigs(func: any): IButtonConfigInterface[] {
    const functionsConfigs: IButtonConfigInterface[] = [];
    func.map((fun) => {
      functionsConfigs.push(this.createButton(fun));
    });

    this.saveButtonsConfigs(functionsConfigs);

    return functionsConfigs;
  }

  /**
   * Create button config
   * @param func
   */
  createButton(func: any): IButtonConfigInterface {
    const buttonFunction = new func();
    const functionConfig: IButtonConfigInterface = {
      name: '',
      handler: () => buttonFunction.execute(this.injector),
      position: buttonFunction.position,
      color: buttonFunction.color,
      role: buttonFunction.role,
      icon: buttonFunction.icon,
    };
    this.translateService.stream(buttonFunction.name).subscribe(vul => functionConfig.name = vul);

    return functionConfig;
  }

  /**
   * Save buttons configs in store
   * @param func
   */
  private saveButtonsConfigs(func: IButtonConfigInterface[]): void {
    const store = this.injector.get(Store);
    store.dispatch(new AppActions.SaveButtonsConfigs(func));
  }

  /**
   * This method find buttons
   * Prop - this is function property such as 'position, name'
   * Value - this is value of searching props of button, such as 'header, logOut'
   * The combination of {prop: position, value: header} return buttons that have position in header
   * @param findBy
   */
  findButtons(findBy: {prop: string, value: string}): Observable<IButtonConfigInterface[]> {
    return this.store.select('appReducer').pipe(
      map((store: IAppReducer) => {
        return store.buttonsConfigs.filter((buttons: IButtonConfigInterface) => buttons[findBy.prop] === findBy.value);
      }))
      .pipe(
        withLatestFrom(this.store.select(store => store.appReducer.authorizedUserData.role)),
        map((vul: [IButtonConfigInterface[], string]) => {
          return vul[0].filter(buttons => buttons.role.includes(vul[1]));
        })
      );
  }
}
