import {IUserRoleInterface} from '../interfaces/IUserRole.interface';

export const UserRole: IUserRoleInterface = {
  ADMIN: 'admin',
  USER: 'user',
  GUEST: 'guest'
};
