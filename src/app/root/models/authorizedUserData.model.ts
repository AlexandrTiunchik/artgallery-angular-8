export class AuthorizedUserDataModel {
  role: string;
  id?: number;
  userName?: string;
  email?: string;
  phoneNumber?: string;
  avatar?: string;
}
