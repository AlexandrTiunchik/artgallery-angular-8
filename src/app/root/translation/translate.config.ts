import {TranslateLoader} from '@ngx-translate/core';
import {createTranslateLoader} from './translate-loader';
import {HttpClient} from '@angular/common/http';

/**
 * Config for translate loader
 * @type {{provide: TranslateLoader; useFactory: (http: HttpClient) => TranslateHttpLoader; deps: HttpClient[]}}
 */
export const translateLoader = {
  provide: TranslateLoader,
  useFactory: (createTranslateLoader),
  deps: [HttpClient]
};
