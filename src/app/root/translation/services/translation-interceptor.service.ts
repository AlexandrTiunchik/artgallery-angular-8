import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {LocalStorageUtil} from '../../util/local-storage.util';
import {apiConfigs} from '../../configs/api.config';

/**
 * Set header for language
 */
@Injectable()
export class TranslationInterceptorService implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const reqs = req.clone({
      headers: req.headers.set('Language', LocalStorageUtil.getUserToken('lang')),
      url: req.url.indexOf('translations') > -1 ? apiConfigs.translationURL : req.url
    });

    return next.handle(reqs);
  }
}
