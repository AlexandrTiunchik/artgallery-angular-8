import {HttpClient} from '@angular/common/http';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {apiConfigs} from '../configs/api.config';

/**
 * This function send a request to NodeJs backend and get list of translations
 * @param {HttpClient} http
 * @returns {TranslateHttpLoader}
 */
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, apiConfigs.translationURL, '');
}
