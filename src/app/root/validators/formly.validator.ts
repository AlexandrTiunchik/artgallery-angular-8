import {FormControl, FormGroup, ValidationErrors} from '@angular/forms';

/**
 * Class for validation functions
 */
export class FormlyValidator {

  /**
   * Min length validator
   * @param minLength
   */
  public static minLengthValidator(minLength: number): null | {} {
    return (control: FormControl): ValidationErrors => {
      return control.value && control.value.length < minLength ? null : {minLength: true};
    };
  }

  /**
   * Max length validator
   * @param maxLength
   */
  public static maxLengthValidator(maxLength: number): null | {} {
    return (control: FormControl): ValidationErrors => {
      return control.value && control.value.length > maxLength ? null : {maxLength: true};
    };
  }

  /**
   * Pattern validator
   * @param pattern
   */
  public static patternValidator(pattern: RegExp): null | {} {
    return (control: FormControl): ValidationErrors => {
      return control.value && !pattern.test(control.value) ? null : {pattern: true};
    };
  }

  /**
   * Confirm password validator
   */
  public static confirmPasswordValidator(): null | {} {
    return (control: FormGroup): ValidationErrors => {
      return control.value && control.value !== control.parent.value.password ? null : {confirmPassword: true};
    };
  }

  /**
   * Require validator
   */
  public static requireValidator(): null | {} {
    return (control: FormControl): ValidationErrors => {
      return control.value && control.value.length <= 0 ? null : {required: true};
    };
  }
}
