import { Injector } from '@angular/core';
import * as AppActions from '../storage/app-storage/actions/app.action';
import { IFunctionInterface } from '../interfaces/IFunction.interface';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { RouterUtil } from '../util/router.util';
import { UserRole } from '../models/role.model';

export class SignInFunction implements IFunctionInterface {

  /**
   * Function name
   * @type {string}
   */
  name: string = 'Sign In';

  position: string = 'header';

  color: string = 'primary';

  role: string[] = [UserRole.GUEST];

  /**
   * This function execute after click on button
   * @param {Injector} injector
   */
  execute(injector: Injector): void {
    const router: Router = injector.get(Router);
    const store = injector.get(Store);
    const url: string = RouterUtil.splitUrl(router.url);

    const checkIsAuthPage = url === '/authorization' ? null : url;

    store.dispatch(new AppActions.SaveLastRoute(checkIsAuthPage));
    router.navigate(['/authorization']).then();
  }
}
