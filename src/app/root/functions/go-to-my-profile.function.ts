import { Injector } from '@angular/core';
import { IFunctionInterface } from '../interfaces/IFunction.interface';
import { UserRole } from '../models/role.model';
import { Router } from '@angular/router';

export class GoToMyProfileFunction implements IFunctionInterface {

  /**
   * Function name
   * @type {string}
   */
  name: string = 'Profile';

  position: string = 'header';

  color: string = null;

  role: string[] = [UserRole.ADMIN, UserRole.USER];

  /**
   * This function execute after click on button
   * @param {Injector} injector
   */
  execute(injector: Injector): void {
    const router: Router = injector.get(Router);
    router.navigate(['/profile/1']).then();
  }
}
