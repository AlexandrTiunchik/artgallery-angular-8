import { Injector } from '@angular/core';
import { Store } from '@ngrx/store';
import * as AppActions from '../storage/app-storage/actions/app.action';
import { IFunctionInterface } from '../interfaces/IFunction.interface';
import { UserRole } from '../models/role.model';

export class LogoutFunction implements IFunctionInterface {

  /**
   * Function name
   * @type {string}
   */
  name: string = 'Logout';

  position: string = 'header';

  color: string = null;

  role: string[] = [UserRole.ADMIN, UserRole.USER];

  /**
   * This function execute after click on button
   * @param {Injector} injector
   */
  execute(injector: Injector): void {
    const store = injector.get(Store);
    store.dispatch(new AppActions.Logout());
  }
}
