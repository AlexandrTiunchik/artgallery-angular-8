export interface IPostInterface {
  id: string;
  author: string;
  avatar: string;
  width: number;
  height: number;
  url: string;
  download_url: string;
}

export interface INewsInterface {
  post: IPostInterface[];
}
