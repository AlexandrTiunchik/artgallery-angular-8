import {FormlyFieldConfig} from '@ngx-formly/core';

export interface IValidatorInterface {
  type: string;
  value?: any;
  validMessage: string;
}

export interface IFormlyBuilderInterface extends FormlyFieldConfig {
  translation: {
    label: string,
    placeholder: string,
  };
  validators: IValidatorInterface[];
}
