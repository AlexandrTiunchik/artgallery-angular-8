export interface IFunctionInterface {
  name: string;
  execute: Function;
  position: string;
  color: string;
  role: string[];
}
