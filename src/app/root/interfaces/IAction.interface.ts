import { Action } from '@ngrx/store';

export interface IActionInterface extends Action {
  payload: any;
}
