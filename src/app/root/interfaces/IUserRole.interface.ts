export interface IUserRoleInterface {
  ADMIN: string;
  USER: string;
  GUEST: string;
}
