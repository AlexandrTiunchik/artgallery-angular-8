export interface IButtonConfigInterface {
  name: string;
  handler: Function;
  position: string;
  color: string;
  role: string[];
  icon?: string;
}
