/**
 * User configs
 */
export const appConfigs = {
  tokenItemName: 'ArtGalleryUserToken',
  language: 'lang'
};
