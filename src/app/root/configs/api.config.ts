/**
 * Api urls
 */
export const apiConfigs = {
  translationURL: 'https://artgalery-nodejs.herokuapp.com/api/translations/'
};
