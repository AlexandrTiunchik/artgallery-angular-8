import { TranslateService } from '@ngx-translate/core';
import { LocalStorageUtil } from '@root';
import { appConfigs } from '../configs/app.config';

export class LanguageUtil {
  public static setDefaultLanguage(translate: TranslateService) {
    if (LocalStorageUtil.getUserToken(appConfigs.language)) {
      translate.setDefaultLang(LocalStorageUtil.getUserToken(appConfigs.language));
    } else {
      LocalStorageUtil.setUserToken(appConfigs.language, 'eng');
      translate.setDefaultLang(LocalStorageUtil.getUserToken(appConfigs.language));
    }
  }

  public static setLanguage(translate: TranslateService, language: string) {
    if (language === LocalStorageUtil.getUserToken(appConfigs.language)) {
      return;
    }

    LocalStorageUtil.setUserToken(appConfigs.language, language);
    translate.setDefaultLang(language);
    location.reload();
  }
}
