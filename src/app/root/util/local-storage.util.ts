/**
 * Class for work with user tokens in local storage
 */
export class LocalStorageUtil {

  /**
   * Get user token
   * @param key
   */
  public static getUserToken(key: string): string {
    return localStorage.getItem(key);
  }

  /**
   * Set user token
   * @param key
   * @param value
   */
  public static setUserToken(key: string, value: string): void {
    localStorage.setItem(key, value);
  }

  /**
   * Remove user token
   * @param key
   */
  public static removeUserToken(key: string): void {
    localStorage.removeItem(key);
  }
}
