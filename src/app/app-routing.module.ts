import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {
    path: '',
    loadChildren: './site/main-page/main.module#MainModule'
  },
  {
    path: 'authorization',
    loadChildren: './site/authorization-page/authorization.module#AuthorizationModule'
  },
  {
    path: 'error',
    loadChildren: './site/error-page/error.module#ErrorModule'
  },
  {
    path: 'news',
    loadChildren: './site/news-page/news-page.module#NewsModule'
  },
  {
    path: 'profile/:id',
    loadChildren: './site/profile-page/profile.module#ProfileModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
